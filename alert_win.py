import ctypes  # An included library with Python install.
import sys
import platform
import os

if platform.system() == "Darwin":
    message = str(sys.argv[1])
    title = "Alert!"
    command = f'''
    osascript -e 'tell app \"System Events\" to display dialog "{message}" with title "{title}"' &> /dev/null
    '''
    os.system(command)
elif platform.system() == "Windows":
    ctypes.windll.user32.MessageBoxW(0, str(sys.argv[1]), "Alert!", 1)