import ctypes
import sys
import subprocess
from lxml import html
import requests
import argparse
import os
import platform

if platform.system() != "Windows":
    path_sep = "\\"
else:
    path_sep = "/"

parser = argparse.ArgumentParser()
# Command line Args
# day
parser.add_argument('-d', dest='day', type=str, required=True, metavar='day', help="specifies day")
# month
parser.add_argument('-m', dest='month', type=str, required=True, metavar='month', help="specifies month")
# year
parser.add_argument('-y', dest='year', type=str, required=True, metavar='year', help="specifies year")
# vaccine
parser.add_argument('-v', dest='vaccine', required=True, choices=['Pfizer', 'Moderna', 'Janssen'], help="determines which vaccine to search for")
# zip
parser.add_argument('-z', dest='zip', type=str, required=True, metavar='zip', help="specifies zip code")
# radius
parser.add_argument('-r', dest='radius', type=str, required=True, choices=['All', '1 mile', '5 miles', '10 miles', '25 miles', '50 miles', '100 miles'], help="specifies search radius")
# location_str
parser.add_argument('-l', dest='location_str', type=str, metavar='location_str', help="specifies location to search for.  Example:'arlington' -optional")

args = parser.parse_args()
#Fix location 
if args.location_str == None:
    args.location_str = ""

#Set the local path
this_path = os.path.dirname(os.path.realpath(__file__))
#print(this_path) 



page = requests.get('https://prepmod.doh.wa.gov/clinic/search?location='+args.zip+'&search_radius='+args.radius+'&q%5Bvenue_search_name_or_venue_name_i_cont%5D='+args.location_str+'&clinic_date_eq%5Byear%5D='+args.year+'&clinic_date_eq%5Bmonth%5D='+args.month+'&clinic_date_eq%5Bday%5D='+args.day+'&q%5Bvaccinations_name_i_cont%5D='+args.vaccine+'&commit=Search#search_results')
tree = html.fromstring(page.content)
location = tree.xpath('//div[@class="md:flex-shrink text-gray-800"]/p[@class="text-xl font-black"]/text()')
available_appointments = tree.xpath('//div[@class="md:flex-shrink text-gray-800"]/p[strong[text()="Available Appointments:"]]/text()[2]')
location = [s.strip() for s in location]
available_appointments = [s.strip() for s in available_appointments]
# debug
# print('location: ',  location)
# print('available_appointments: ', available_appointments)
# print('\n')
show_no_appointments = True
for i in range(len(available_appointments)):
    if int(available_appointments[i]) > 0:
        show_no_appointments = False
        p = subprocess.Popen([sys.executable, this_path + path_sep + "alert_win.py", location[i] + " has " + available_appointments[i] + " appointment(s)"])
        print(location[i] + " has " + available_appointments[i] + " appointment(s)")
if show_no_appointments:
    print("No appointments  :-(")
    # p = subprocess.Popen(
    #     [sys.executable, this_path + path_sep + "alert_win.py", "No appointments  :-("])

